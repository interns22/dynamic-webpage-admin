import React, { createContext, useEffect, useState, useRef } from "react";
import { Spin, message } from "antd";
export const UserAuthenticationContext = createContext();
export default function UserAuthenticationContextProvider({ children }) {
  // const [accessToken,setAccessToken]=useState(localStorage.getItem('accessToken')||'')
  const [accessToken, setAccessToken] = useState(
    "eyJraWQiOiI4dEt0QVQ2ZnoyUVZTS2d4ZmxcL2Nvd2xpR29mdUVZZUt3Q1Z0Ymt1cXhxbz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJkZDljZDgzNi05OGMzLTQ5ZjMtOGM0MC0xMjhlMjNiM2FiMGQiLCJldmVudF9pZCI6IjhmNGRhMDAyLTMzNjQtNDZkMy04MzczLTgyN2FhYTFhMGM2NiIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE2NTQzNDA2ODgsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5hcC1zb3V0aC0xLmFtYXpvbmF3cy5jb21cL2FwLXNvdXRoLTFfVGRUMmlLNUdFIiwiZXhwIjoxNjU0MzQ0Mjg4LCJpYXQiOjE2NTQzNDA2ODgsImp0aSI6IjBmMGJkMTdmLTFjNjQtNGJkMy1hMDlhLTI3ZGIxMjNjMjA2NCIsImNsaWVudF9pZCI6Ijd2b3VxNHUyOTEyZnB0MmpvMmMwNWFyajNrIiwidXNlcm5hbWUiOiJzdWJhc2gifQ.d_qireP5pxf9FoqLJ098qy5P85xNWvEF_ONVLhYgP4o3YKFaIKrhzb_GDueqecOMv3xYl-VQ71Kb4E-WErsR7uuQEhN4vBcL2VCma5WAt6nTDlMJNyspXM7R461CbqVFvHwy_NNI6CDaBx4jt1qSTbid8tIWvy97ROEeakogcZqJawdYVwSaB9ySz1hX1HXinz1FlxO0LNdZ6D8FIdnVgzUDg8n5AoRKB2PbudmESJX9AYj__kXjD1umQ52BvD6vEEyqb-3HawvHQBt1mFBbM_3n7fIeaN0SiTe8G7R6eUBBvDb8wx67qxlhCsok2R5Y1Z7ZMuIH3rUXtra7RTMyjw"
  );
  const [userLoading, setUserLoading] = useState(true);
  const PRIMARY_APP_NAME = "KUMARI_KALVI_APP_DEV";
  const PRIMARY_API =
    "https://pnre7lwmvk.execute-api.ap-south-1.amazonaws.com/dev";
  const [userInformation, setUserInformation] = useState("");
  const [wakeupFunc, setWakeupfunc] = useState(false);

  useEffect(() => {
    setUserLoading(true);
    if (accessToken === "") {
      window.location.href = "/signin";
      return;
    }
    var adminVerifyRaw = JSON.stringify({
      appName: PRIMARY_APP_NAME,
      modName: "VERIFY_TOKEN",
    });
    var adminVerifyRequestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
      body: adminVerifyRaw,
      redirect: "follow",
    };
    fetch(`${PRIMARY_API}/general`, adminVerifyRequestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.statusCode === 200) {
          // setUserLoading(false)
          var adminVerifyRaw2 = JSON.stringify({
            appName: "INTERNS_APP_DEV",
            modName: "GET_DATA_INTERNS_AC",
            elementId: "ac-service-form",
            attributeName: "dishes_feedback",
          });
          var adminVerifyRequestOptions = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: adminVerifyRaw2,
            redirect: "follow",
          };
          fetch(`${PRIMARY_API}/general`, adminVerifyRequestOptions)
            .then((response2) => response2.json())
            .then((result2) => {
              console.log(result2);
              if (result2.statusCode === 200) {
                setUserInformation(result2.body);
                setUserLoading(false);
              }
            })
            .catch((e) => {
              console.log(e);
            });
        } else {
          setAccessToken("");
          localStorage.removeItem("accessToken");
          window.location.href = "/signin";
        }
      })
      .catch((e) => {
        window.location.href = "/signin";
        localStorage.removeItem("accessToken");
      });
  }, [accessToken]);
  const deleteItem = (value, i) => {
    const raw = JSON.stringify({
      appName: PRIMARY_APP_NAME,
      modName: "UPDATE_A_PROTECTED_DATA_INTERNS",
      id: "ac-service-form",
      attributeName: "dishes",
      attributeValue: userInformation.dishes.filter((e, ind) => ind !== i),
    });
    var requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
      body: raw,
      redirect: "follow",
    };
    fetch(`${PRIMARY_API}/general`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.statusCode === 200) {
          userInformation.dishes = userInformation.dishes.filter(
            (e, ind) => ind !== i
          );
          setUserInformation(userInformation);
          setWakeupfunc(!wakeupFunc);
          message.success("Deleted");
        }
      });
  };
  if (userLoading)
    return (
      <div className="text-center">
        <Spin />
      </div>
    );
  return (
    <UserAuthenticationContext.Provider
      value={{
        accessToken,
        PRIMARY_APP_NAME,
        PRIMARY_API,
        userInformation,
        deleteItem,
        wakeupFunc,
         setWakeupfunc
      }}
    >
      {children}
    </UserAuthenticationContext.Provider>
  );
}
