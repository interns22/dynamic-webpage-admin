import React from 'react'
import'./Navbar.css'
import {Link} from "react-router-dom"

const Navbar = () => {
  return (
    <> 
    <div className='sidebar'>
        <div className='sidebar-brand'>
            <h2><span className='nav'></span>Caffena</h2>
        </div>

        <div className='sidebar-menu'>
           <ul>
               <li>
                   <Link to = "/dashboard" className='active'><span className="fas fa-igloo"></span>
                   <span>Dashboard</span></Link>
               </li>
               <li>
                   <Link to ='/products'><span className="fas fa-shopping-bag"></span>
                   <span>Add-Products</span></Link>
               </li>
               <li>
                   <Link to ='/edit'><span className="fas fa-shopping-bag"></span>
                   <span> View-Products</span></Link>
               </li>
               <li>
                   <Link to ='/messages'><span className="fas fa-envelope"></span>
                   <span>Messages</span></Link>
               </li>
           </ul>
        </div>
    </div>
    </>
  )
}
export default Navbar