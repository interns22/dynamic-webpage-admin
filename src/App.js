import './App.css';
import 'antd/dist/antd.css';
import Navbar from './Components/Navbar';
import { BrowserRouter as Router,Routes,Route} from 'react-router-dom'
import Products from './Sections/Products';
import Messages from './Sections/Messages';
import Account from './Sections/Account';
import Dashboard from './Sections/Dashboard';
import Productsedit from './Sections/Productsedit';
import UserAuthenticationContextProvider from './Context/UserDetailsContext';
import Updateproduct from './Sections/Updateproduct';


function App() {
  return (
    <>
    <UserAuthenticationContextProvider>
    <Router>
    <Navbar />
    
    <Routes>
      
      {/* <Route path='/' exact element={<Dashboard />} /> */}
      <Route path='/dashboard' exact element={<Dashboard />} />
      <Route path='/products' exact element={<Products />} />
      <Route path='/edit' exact element={<Productsedit />} />
      <Route path='/messages' exact element={<Messages />} />
      <Route path='/account' exact element={<Account />} />
      <Route path='/updateproduct' exact element={<Updateproduct />} />
    </Routes>
    </Router>
    </UserAuthenticationContextProvider>
    </>
    
      
  );
}

export default App;
