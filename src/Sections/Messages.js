import React from 'react';
import './Messages.css'
import { Table } from 'antd';
import { useContext } from 'react';
import { UserAuthenticationContext } from '../Context/UserDetailsContext';

const TableComponent3 = () => {
    const { userInformation } = useContext(UserAuthenticationContext)
    const columns = [
        { title: <h2>Name</h2>, dataIndex: 'name', key: 'name' },
        Table.EXPAND_COLUMN,

        { title: <h2>Email</h2>, dataIndex: 'email', key: 'email' },
        Table.SELECTION_COLUMN,

        { title: <h2>Message</h2>, dataIndex: 'feedback', key: 'feedback' },

    ];

    return (
        <div className='messageform'>
            {/* <h1>Message</h1> */}
            <Table

                columns={columns}

                expandable={{
                    expandedRowRender: record => <p style={{ margin: 0 }}>{record.description}</p>,
                }}
                dataSource={userInformation.dishes_feedback}
            />

        </div>
    )


}

export default TableComponent3;


