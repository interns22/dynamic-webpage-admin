import React, { useContext, useEffect,useState } from "react";
import "./Productedit.css";
import DATA from "./Data";
import { Button } from "antd";
import { NavLink } from "react-router-dom";
import { UserAuthenticationContext } from "../Context/UserDetailsContext";
const Productsedit = () => {
  const { userInformation, deleteItem, wakeupFunc, setWakeupfunc } = useContext(
    UserAuthenticationContext
  );
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    setTimeout(()=>{
setLoading(false)
    },0.0005)
  }, []);
  const handleDelete = (item, i) => {
    deleteItem(item, i);
  };
  const cardItem = (item, index) => {
    return (
      
      <div className="card  my-5 py-4" key={item.id} style={{ width: "30rem" }}>
        <img src={item.image} className="card-img-top" alt={item.title} />
        <div className="card-body text-center">
          <h5 className="card-title">{item.title}</h5>
          <p className="lead">{item.price}</p>
          <NavLink to="" className="btn btn-outline-primary">
            update
          </NavLink>
          <span>
            {" "}
            <Button
              type="primary"
              onClick={() => handleDelete(item, index)}
              size="small"
              className="btn btn-outline-primary"
            >
              delete
            </Button>
          </span>
        </div>
      </div>
    );
  };

  return (
    <div>
      <div className="container py-25">
        <div className="row">
          <div className="col-10 text-center">
            <h1>Product</h1>
            <hr />
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row justify-content-around">
          {userInformation.dishes.map(cardItem)}
        </div>
      </div>
    </div>
  );
};

export default Productsedit;
