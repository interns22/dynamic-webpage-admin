import React from "react";
import { NavLink } from "react-router-dom";

const Account = () => {
  return (
    <>
      <div className="main-content">
        <header>
          <h2>
            <label htmlFor="">
              <span className="fas fa-bars"> ACCOUNT</span>
            </label>
          </h2>

          <button
            type="button"
            class="btn btn-primary"
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
          >
          Logout
          </button>
              
        </header>
      </div>
    </>
  );
};

export default Account;
