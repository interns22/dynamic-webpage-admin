import React from 'react'
import './Dashboard.css'

const Dashboard = () => {
  return (
    <>
      <div className='main-content'>
        <header>
            <h2>
                <label htmlFor=''>
                    <span className='fas fa-bars'> Dashboard</span>
                </label>
            </h2>

            <div className='Account'>
                <span className='fas fa-user'> Admin </span>
            </div>
        </header>

        <main>
            <div className='cards'>
            <div className='card-single'>
                <div>
                    <h1>0</h1>
                    <span>Admin</span>
                </div>
                <div>
                    <span className='fas fa-users'></span>
                </div>
             </div>

             <div className='card-single'>
                <div>
                    <h1>0</h1>
                    <span>Products</span>
                </div>
                <div>
                    <span className='fas fa-shopping-bag'></span>
                </div>
             </div>

             <div className='card-single'>
                <div>
                    <h1>0</h1>
                    <span>messages</span>
                </div>
                <div>
                    <span className='fas fa-envelope'></span>
                </div>
             </div>

            </div>
        </main>
    </div>
    </>
  )
}


export default Dashboard