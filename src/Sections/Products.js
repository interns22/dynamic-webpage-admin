import React,{useContext,useState} from "react";
import {
  Button,
  Form,
  Input,
  UploadProps,
  Upload,
  message,
  Checkbox,
  Alert,
  Image
} from "antd";
import axios from 'axios'
import ImgCrop from 'antd-img-crop';
import "./Products.css";
import { UploadOutlined } from '@ant-design/icons';
import {UserAuthenticationContext} from '../Context/UserDetailsContext'
import { useForm } from "rc-field-form";

// const props: UploadProps = {
//   name: 'file',
//   action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
//   headers: {
//     authorization: 'authorization-text',
//   },
//   onChange(info) {
//     if (info.file.status !== 'uploading') {
//       console.log(info.file, info.fileList);
//     }
//     if (info.file.status === 'done') {
//       message.success(`${info.file.name} file uploaded successfully`);
//     } else if (info.file.status === 'error') {
//       message.error(`${info.file.name} file upload failed.`);
//     }
//   },
// };



const Products = () => {
  const { TextArea } = Input;
  const [form]=useForm()
const {PRIMARY_APP_NAME,PRIMARY_API,accessToken}=useContext(UserAuthenticationContext)
const [imageUrl,setImageUrl]=useState("")
const [loading,setLoading]=useState(false)
const [tempFile,settempFile]=useState("")
const [errMess,setErrMess]=useState("")
const handleFinish = (values) => {
  values.image=imageUrl
  var adminVerifyRaw = JSON.stringify({ "appName": "INTERNS_APP_DEV", "modName": "CREATE_APPOINTMENT", attributeName: "dishes", attributeValue: [values] })

var adminVerifyRequestOptions = {

method: 'POST',

headers: { "Content-Type": "application/json" },

body: adminVerifyRaw,

redirect: 'follow'

};

fetch(`${PRIMARY_API}/general`, adminVerifyRequestOptions)

.then(response => response.json())

.then(result => {
console.log(result)
if (result.statusCode === 200) {

message.success("Submitted successfully")

}

})

.catch(err => console.log(err))

};

const uploadProps = {

  onRemove: () => {
      var raw = JSON.stringify({ "appName": PRIMARY_APP_NAME, "modName": "DELETE_FILE", "fileName": `/proof.pdf` });
      var requestOptions = {
          method: 'POST',
          headers: { "Content-Type": "application/json" },
          body: raw,
          redirect: 'follow'
      };
      fetch(`${PRIMARY_API}/general`, requestOptions)
          .then(response => response.json())
          .then(result => {
              if (result.statusCode === 200) {
                  setImageUrl('')
              } else {
                  message.error('Failed to delete', 5)
              }
          })
          .catch(error => { message.error('Failed to delete', 5) })
  },
  beforeUpload: file => {
      const isPdf =  (file.type === 'image/jpeg') || (file.type === 'image/png') || (file.type === 'image/jpg')
      if (!isPdf) {
          setErrMess('You can only upload PDF/JPEG/PNG file !')
          message.error('You can only upload PDF/JPEG/PNG file');
      }
      const isLt5M = file.size / 1024 / 1024 < 5;
      if (!isLt5M) {
          setErrMess('File must be smaller than 5MB!')
          message.error('File must be smaller than 5MB!');
      }
      return isPdf && isLt5M;
  },
  customRequest: (value) => {
    const getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    }
    getBase64(value.file, image => {
        //Execute Save Function
        axios({
            url: `https://57mpla6pti.execute-api.ap-south-1.amazonaws.com/dev/secure/upload-to-gallery`,
            method: 'POST',
            headers: { "Content-Type": "application/json", "Authorization": `Bearer ${accessToken}` },
            data: JSON.stringify({ "appName": "BUBBLY_BEE_INSIGHTS", "s3folder": `dishes` })
        })

            .then(result2 => {
                if (result2.data.body.url) {
                    var file2 = value.file;
                    var requestOptions3 = {
                        method: 'PUT',
                        headers: { "Content-Type": value.file.type },
                        data: file2
                    };
                    axios(result2.data.body.url, requestOptions3)
                        .then(response3 => response3.json())
                        .then(result3 => {
                            // updateProfilePic(result.data.body.link)
                        })
                        .catch(error => {
                         
                          setImageUrl(result2.data.body.link)    
                        });
                }
            })
            .catch(error => { message.error('upload failed', 5); message.destroy("uploading"); console.log(error) });
    })
}
}
  return (
    <>
    <div className='main-content'>
        <header>
            <h2>
                <label htmlFor=''>
                    <span className='fas fa-bars'> Add Products</span>
                </label>
            </h2>

            <div className='Account'>
                <span className='fas fa-user'> Admin</span>
            </div>
        </header>
    
      <div className="Productsform">
        <Form onFinish={handleFinish} className="form">
          <div className="container">
            <h2>Add New Product</h2>
            <Form.Item name="name">
              <Input className="box" placeholder="Enter Product Name" />
            </Form.Item>

            <Form.Item name="price">
              <Input className="box" placeholder="Enter Product Price" />
            </Form.Item>
            <Form.Item name="details">
              <TextArea className="box" placeholder="Enter Product Details" />
            </Form.Item>
            {/* <div className='box'><Upload {...props}>
                <Button icon={<UploadOutlined />}>Choose File</Button>
              </Upload>
              </div> */}
            <Form.Item name="image" >
            <ImgCrop rotate aspect={16/9}>
              <Upload
                showUploadList={false}
                accept=".jpeg,.jpg,.png"
                {...uploadProps}
              >
                <div className="d-md-flex">
                  {!imageUrl&&<Button icon={<UploadOutlined />}>Upload Image</Button>}
                  {imageUrl&&<Image src={imageUrl}></Image>}
                  <div className="mt-md-0 mt-2 ms-2 ">
                    {!errMess &&
                      "*jpeg/png file less than 5 MB can be uploaded"}
                    {errMess && (
                      <Alert
                        closable={false}
                        banner
                        message={errMess}
                        type="error"
                      />
                    )}
                  </div>
                </div>
              </Upload>
              </ImgCrop>
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Add Product
              </Button>
            </Form.Item>
          </div>
        </Form>

      </div>
      </div>
    </>
  );
};

export default Products;
