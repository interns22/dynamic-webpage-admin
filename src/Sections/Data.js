
const DATA = [
    {
        id : 0,
        title : "Arabica Beans",
        price : '₹250',
        img : "/Images/menu-11.jpg"
    },
    {
        id : 1,
        title : "Jamaican Blue Mountain Beans",
        price : '₹350',
        img : "/Images/menu-12.jpg"
    },
    {
        id : 2,
        title : "Jember Beans",
        price :'₹450',
        img : "/Images/menu-3.jpg"
    },
    {
        id : 3,
        title : " Kent Beans",
        price : '₹50',
        img : "/Images/menu-4.jpg"
    },
    {
        id : 4,
        title : "Liberica Beans",
        price : '₹45',
        img : "/Images/menu-5.jpg"
    },
    {
        id : 5,
        title : "Xxxx",
        price : '₹450',
        img : "/Images/menu-1.jpg"
    },
    {
        id : 6,
        title : "Xxxx",
        price : '₹450',
        img : "/Images/product-1.jpg"
    },
    {
        id : 7,
        title : "Xxxx",
        price : '₹450',
        img : "/Images/product-2.jpg"
    },
    {
        id : 8,
        title : "Xxxx",
        price : '₹450',
        img : "/Images/product-3.jpg"
    },

]
export default DATA